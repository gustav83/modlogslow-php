<?php

$file = './slow_log';
$content = file_get_contents($file);

$lines = explode(PHP_EOL, $content);

$keys = array('average', 'elapsed', 'cpu', 'ip', 'host', 'reqinfo');
$rows = array();

printf('<h1>Stats from %d lines (%s)</h1>', count($lines), date(DATE_ATOM, filemtime($file)));

foreach($lines as $line) {
    $parts = explode(':', $line);
    $line = str_replace('reqinfo: GET ', 'reqinfo: GET=', $line);
    $line = str_replace('reqinfo: POST ', 'reqinfo: POST=', $line);
    $line = str_replace(' ', ':', $line);
    $line = str_replace('::', ':', $line);
    $row = array();
    if(preg_match_all('/(?<key>elapsed|cpu|ip|host|reqinfo):(?<value>[^:]+)/', $line, $matches)) {
        foreach($matches['key'] as $i => $key) {
            $value = $matches['value'][$i];
            if($key == 'cpu' && preg_match_all('/(\d+\.\d+)/', $value, $m)) {
                $value = 0.0;
                foreach($m[0] as $k => $v) {
                    $value += (float)$v;
                }
            }
            if($key == 'elapsed') {
                $value = (float)$value;
            }
            $row[$key] = $value;
        }
    }
    if($row) {
        $row['average'] = $row['cpu'] / $row['elapsed'];
        $rows[] = $row;
    }
}

$sort = isset($_GET['sort']) ? $_GET['sort'] : 'average';
usort($rows, function($a, $b) use($sort) {
    if(is_float($a[$sort])) {
        return $a[$sort] - $b[$sort] < 0 ? 1 : -1;
    }
    return strcmp($a[$sort], $b[$sort]);
});

echo '<table style="width:100%">';

foreach($keys as $k) {
    printf('<th><a href="?sort=%s">%1$s</a></th>', $k);
}

foreach($rows as $row) {
    echo '<tr>';
    foreach($keys as $k) {
        $value = $row[$k];
        if(in_array($k, array('cpu', 'elapsed', 'average'))) {
            $value = round($value, 5);
        }
        printf('<td>%s</td>', $value);
    }
    echo '</tr>';
}

echo '</table>';
